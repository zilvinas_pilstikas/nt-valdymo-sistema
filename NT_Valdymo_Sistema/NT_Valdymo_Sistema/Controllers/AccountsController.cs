﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Valdymo_Sistema.Models;
using System.Data.Entity;
namespace NT_Valdymo_Sistema.Controllers
{
    public class AccountsController : Controller
    {
        private REDatabaseEntities db = new REDatabaseEntities();

        // GET: Accounts
        public ActionResult AccountsDisplay()
        {
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var users = new List<User>();
                users = dc.User.Where(a => a.UserType.Equals("Klientas")).ToList();
                return View(users);
            }
        }

        [HttpPost]
        public ActionResult AccountsDisplay(string o)
        {
            var ID = Convert.ToInt32(Request.Form["ID"]);
            var busena = Request.Form["Busena"];
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                User user = dc.User.Where(a => a.UserID == ID).First();
                user.RegistrationState = busena;
                user.ConfirmPassword = user.Password;
                user.EditingDate = System.DateTime.Now;
                dc.User.Attach(user);
                dc.Entry(user).State = EntityState.Modified;
                dc.SaveChanges();
                var users = new List<User>();
                users = dc.User.Where(a => a.UserType.Equals("Klientas")).ToList();
                return View(users);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (!user.UserType.Equals("Klientas"))
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            var id = Convert.ToInt32(Request.Form["userID"]);
            User oldUser = db.User.Find(id);

            oldUser.EditingDate = System.DateTime.Now;
            oldUser.RegistrationDate = oldUser.RegistrationDate;
            oldUser.RegistrationState = oldUser.RegistrationState;
            oldUser.Password = oldUser.Password;
            oldUser.ConfirmPassword = oldUser.Password;
            oldUser.UserType = oldUser.UserType;
            oldUser.Address = user.Address;
            oldUser.City = user.City;
            oldUser.Email = user.Email;
            oldUser.Name = user.Name;
            oldUser.Surname = user.Surname;
            oldUser.UserType = oldUser.UserType;

            db.User.Attach(oldUser);
            db.Entry(oldUser).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("AccountsDisplay", "Accounts");
        }
    }
}