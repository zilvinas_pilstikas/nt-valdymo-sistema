﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Valdymo_Sistema.Models;
using System.Data.Entity;
using System.Net;
using System.Drawing;
using System.IO;
using System.Data.SqlTypes;

namespace NT_Valdymo_Sistema.Controllers
{

    public class ReportController : Controller
    {
        private REDatabaseEntities db = new REDatabaseEntities();

        // GET: AdList
        public ActionResult AccountantMainWindow()
        {
            return View();
        }

        public ActionResult AdViewWindow()
        {
            return View(db.Advertisement.ToList());
        }

        [HttpGet]
        public ActionResult AdReportWindow()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdReportWindow(AdReport adReport)
        {
            string message = "";

            if (ModelState.IsValid)
            {
                using (REDatabaseEntities dc = new REDatabaseEntities())
                {
                    message = "Sudaryta ataskaita!";
                    int priceFrom = Convert.ToInt32(adReport.PriceFrom);
                    int priceTo = Convert.ToInt32(adReport.PriceTo);
                    var neededAds = new List<Advertisement>();
                    var neededAds2 = new List<Advertisement>();
                    var neededAds3 = new List<Advertisement>();
                    var cities = new List<String>();
                    var count = new List<int>();
                    var ads = dc.Advertisement;
                    var houses = dc.House;

                    foreach (var ad in ads)
                    {
                        neededAds2.Add(dc.Advertisement.Where(a => a.HouseID == ad.House.HouseID).First());
                    }

                    neededAds3 = (neededAds2.Where(a => a.Price <= priceTo).ToList());
                    neededAds = (neededAds3.Where(a => a.Price >= priceFrom).ToList());

                    cities = neededAds.GroupBy(x => x.House.Municipality).Where(group => group.Count() > 0).Select(group => group.Key).ToList();

                    count = neededAds.GroupBy(x => x.House.Municipality).Where(group => group.Count() > 0).Select(group => group.Count()).ToList();

                    var all = neededAds.GroupBy(i => i.House.Municipality).Select(g => new { Municipality = g.Key, Count = g.Count(), Average = g.Average(i => i.Price) }).ToString();

                    var average = neededAds.GroupBy(i => i.House.Municipality).Where(group => group.Count() > 0).Select(g => new { Average = g.Average(i => i.Price)});

                    ViewBag.cities = cities;
                    ViewBag.count = count;
                    ViewBag.average = average;
                    ViewBag.all = all;
                }
            }

            ViewBag.Message = message;

            return View(adReport);
        }

        public ActionResult SpecificAdWindow(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisement.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        public ActionResult REReportWindow()
        {
            return View();
        }

        public ActionResult REViewWindow()
        {
            return View();
        }

        public ActionResult SpecificREWindow(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Declaration declaration = db.Declaration.Find(id);
            if (declaration == null)
            {
                return HttpNotFound();
            }
            return View(declaration);
        }

        [HttpGet]
        public ActionResult AdFilterWindow()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdFilterWindow(AdReport adReport)
        {
            string message = "";

            if (ModelState.IsValid)
            {
                using (REDatabaseEntities dc = new REDatabaseEntities())
                {
                    message = "Filtro nustatymai atlikti!";
                    int priceFrom = Convert.ToInt32(adReport.PriceFrom);
                    int priceTo = Convert.ToInt32(adReport.PriceTo);
                    var neededAddresses = new List<String>();
                    var neededHouse = new List<String>();
                    var ads = new List<Advertisement>();
                    var neededAds = new List<Advertisement>();
                    var houses = new List<House>();

                    ads = dc.Advertisement.Where(x => x.Price <= priceTo).ToList();
                    neededAds = ads.Where(x => x.Price >= priceFrom).ToList();

                    foreach (var ad in neededAds)
                    {
                        houses.Add(dc.House.Where(a => a.HouseID == ad.HouseID).First());
                    }

                    for (int i = 0; i < houses.Count; i++)
                    {
                        neededAddresses.Add(houses[i].Settlement + ", " + houses[i].Street + " " + houses[i].HouseNr);
                        neededHouse.Add(houses[i].BuildingType);
                    }

                    ViewBag.neededAds = neededAds;
                    ViewBag.neededAddress = neededAddresses;
                    ViewBag.neededHouse = neededHouse;
                }
            }
            ViewBag.Message = message;
            return View(adReport);
        }

        public ActionResult REFilterWindow()
        {
            return View();
        }
    }
}