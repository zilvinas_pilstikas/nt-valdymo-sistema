﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Valdymo_Sistema.Models;
using System.Data.Entity;
using System.Net;
using System.Drawing;
using System.IO;
using System.Data.SqlTypes;

namespace NT_Valdymo_Sistema.Controllers
{
    public class AdSubsystemController : Controller
    {

        private REDatabaseEntities db = new REDatabaseEntities();
        // GET: AdSubsystem

        public ActionResult ChooseAdType()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Advertisement()
        {
            var tuple = new Tuple<Advertisement, House>(new Advertisement(), new House());
            return View(tuple);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Advertisement(Tuple<Advertisement, House> Ad, HttpPostedFileBase image
            )
        {

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    String fileName = Path.GetFileNameWithoutExtension(image.FileName);
                    String extension = Path.GetExtension(image.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    Ad.Item1.ImagePath = "~/Image/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                    image.SaveAs(fileName);
                    Ad.Item1.ImageTitle = "SkelbimoFoto";
                }
                Ad.Item1.CreationDate = System.DateTime.Now;
                Ad.Item1.EditingDate = System.DateTime.Now;
                Ad.Item1.UserID = (int)Session["userID"];
                Ad.Item1.MapID = 1;
                Ad.Item2.Declarated = 0;
                db.House.Add(Ad.Item2);
                db.SaveChanges();
                var last = db.House.OrderByDescending(p => p.HouseID).Take(1).Single();

                Ad.Item1.HouseID = last.HouseID;


                db.Advertisement.Add(Ad.Item1);
                db.SaveChanges();


                return RedirectToAction("AdvertisementMain", "AdSubsystem");

            }

            return View(Ad);
        }
        public ActionResult AdvertisementList()
        {

            return View(db.Advertisement.ToList());
        }
        public ActionResult markAd(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisement.Find(id);
            House house = db.House.Find(advertisement.HouseID);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            Tuple<Advertisement, House> Ad = new Tuple<Advertisement, House>(advertisement, house);
            return View(advertisement);
        }
        
        [HttpPost, ActionName("markAd")]
        [ValidateAntiForgeryToken]
        public ActionResult MarkConfirmed(int id)
        {
            MarkedAdvertisements mA = new MarkedAdvertisements();
            Advertisement advertisement = db.Advertisement.Find(id);
            mA.AdvertisementID = advertisement.AdvertisementID;
            mA.UserID = (int)Session["userID"];
            mA.WhenMarked = System.DateTime.Now;    
            db.MarkedAdvertisements.Add(mA);
            db.SaveChanges();
            return RedirectToAction("AdvertisementList");
        }
        
    

    public void answerRequest()
        {

        }

        public ActionResult MarkedAdListWindow()
        {
            var query = "Select * from MarkedAdvertisements where UserID ="+Session["userID"];
            return View(db.MarkedAdvertisements.SqlQuery(query));
        }

        public void getMarkedAdListData()
        {

        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisement.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        public ActionResult AdvertisementMain()
        {
            return View();
        }

        public void getMapData()
        {

        }

        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement add = db.Advertisement.Find(id);
            House house = db.House.Find(add.HouseID);

            if (add == null || house == null)
            {
                return HttpNotFound();
            }
            Tuple<Advertisement, House> Ad = new Tuple<Advertisement, House>(add, house);
            return View(Ad);
        }

        // POST: Advertisements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tuple<Advertisement, House> Ad, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    String fileName = Path.GetFileNameWithoutExtension(image.FileName);
                    String extension = Path.GetExtension(image.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    Ad.Item1.ImagePath = "~/Image/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                    image.SaveAs(fileName);
                    Ad.Item1.ImageTitle = "SkelbimoFotoAtnaujinta";
                }
                Ad.Item1.HouseID = Ad.Item2.HouseID;
                Ad.Item1.EditingDate = System.DateTime.Now;

                db.Entry(Ad.Item1).State = EntityState.Modified;
                db.Entry(Ad.Item2).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AdvertisementList");
            }
            return View(Ad);
        }
 

        public ActionResult DeleteMarkedAd(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarkedAdvertisements mA = db.MarkedAdvertisements.Find(id);
            if(mA == null)
            {
                return HttpNotFound();
            }

            return View(mA);
        }
        [HttpPost, ActionName("DeleteMarkedAd")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteMarkedConfirmed(int id)
        {
            MarkedAdvertisements mA = db.MarkedAdvertisements.Find(id);

            db.MarkedAdvertisements.Remove(mA);
            db.SaveChanges();
            return RedirectToAction("MarkedAdListWindow");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisement.Find(id);
            House house = db.House.Find(advertisement.HouseID);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            Tuple<Advertisement, House> Ad = new Tuple<Advertisement, House>(advertisement, house);
            return View(advertisement);
        }

        // POST: Advertisements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Advertisement advertisement = db.Advertisement.Find(id);
            House house = db.House.Find(advertisement.HouseID);
            db.Advertisement.Remove(advertisement);
            db.House.Remove(house);
            db.SaveChanges();
            return RedirectToAction("AdvertisementList");
        }
    }
}