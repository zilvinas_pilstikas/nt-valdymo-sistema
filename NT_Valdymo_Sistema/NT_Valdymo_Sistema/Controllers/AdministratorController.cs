﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using NT_Valdymo_Sistema.Models;
using System.Web.Mvc;

namespace NT_Valdymo_Sistema.Controllers
{
    public class AdministratorController : Controller
    {
        // GET: Administrator
        public ActionResult MainAdministrator()
        {
            return View();
        }

        public ActionResult UsersList(int ?id)
        {
            var users = new List<User>();
            using (REDatabaseEntities db = new REDatabaseEntities())
            {
                
                users = db.User.ToList();

                ViewBag.viewOrEdit = id;
                ViewBag.users = users;
            }

            return View();
        }

        [HttpGet]
        public ActionResult DetailedUserInformation(int ?id)
        {
            REDatabaseEntities db = new REDatabaseEntities();
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var user = db.User.Find(id);

            return View(user);
        }

        [HttpPost]
        public ActionResult DetailedUserInformation(User user)
        {
            bool successStatus = false;
            string message = "";

            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var updatedUser = new User();
                // 11
                updatedUser.UserID = user.UserID;
                updatedUser.Password = user.Password;
                updatedUser.ConfirmPassword = user.Password;
                updatedUser.Name = user.Name;
                updatedUser.Surname = user.Surname;
                updatedUser.Email = user.Email;
                updatedUser.City = user.City;
                updatedUser.Address = user.Address;
                updatedUser.RegistrationDate = user.RegistrationDate;
                updatedUser.EditingDate = user.EditingDate;
                updatedUser.RegistrationState = user.RegistrationState;
                updatedUser.UserType = user.UserType;

                dc.Entry(updatedUser).State = EntityState.Modified;
                dc.SaveChanges();
                message = "Sėkmingai išsiuntėtę įvertinimą!";
                successStatus = true;
            }
            
            ViewBag.Message = message;
            ViewBag.Status = successStatus;
            return View();
        }
    }
}