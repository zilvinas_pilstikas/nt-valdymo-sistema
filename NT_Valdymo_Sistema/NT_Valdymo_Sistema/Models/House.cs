
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace NT_Valdymo_Sistema.Models
{

using System;
    using System.Collections.Generic;
    
public partial class House
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public House()
    {

        this.Advertisement = new HashSet<Advertisement>();

        this.Declaration = new HashSet<Declaration>();

    }


    public int HouseID { get; set; }

    public string Municipality { get; set; }

    public string Settlement { get; set; }

    public string Microdistrict { get; set; }

    public string Street { get; set; }

    public Nullable<int> HouseNr { get; set; }

    public Nullable<int> HouseArea { get; set; }

    public Nullable<int> RoomNr { get; set; }

    public Nullable<int> PlotArea { get; set; }

    public Nullable<int> FloorNr { get; set; }

    public Nullable<int> YearBuilt { get; set; }

    public string BuildingType { get; set; }

    public string HouseEquipment { get; set; }

    public string HeatingType { get; set; }

    public string AdditonalInfo { get; set; }

    public Nullable<int> Declarated { get; set; }



    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<Advertisement> Advertisement { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<Declaration> Declaration { get; set; }

}

}
