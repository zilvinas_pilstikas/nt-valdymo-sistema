﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NT_Valdymo_Sistema.Models
{
    public class AdReport
    {
        [Display(Name = "Kaina nuo (€)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įrašykite kainą")]
        public double PriceFrom { get; set; }

        [Display(Name = "Kaina iki (€)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įrašykite kainą")]
        public double PriceTo { get; set; }
    }
}