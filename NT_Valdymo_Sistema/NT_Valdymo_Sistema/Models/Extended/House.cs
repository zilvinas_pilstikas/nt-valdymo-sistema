﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace NT_Valdymo_Sistema.Models
{
    [MetadataType(typeof(HouseMetadata))]
    public partial class House
    {

    }

    public class HouseMetadata
    {
        [Display(Name = "Savivaldybė")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite savivaldybę")]
        public string Municipality { get; set; }

        [Display(Name = "Gyvenvietė")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite savo gyvenvietę")]
        public string Settlement { get; set; }

        [Display(Name = "Mikrorajonas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite mikrorajoną")]
        public string Microdistrict { get; set; }

        [Display(Name = "Gatvė")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite gatvę")]
        public string Street { get; set; }

        [Display(Name = "Namo numeris")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite namo numerį")]
        public string HouseNr { get; set; }

        [Display(Name = "Namo plotas(kv/m)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite namo plotą")]
        public string HouseArea { get; set; }

        [Display(Name = "Kambarių skaičius")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite kambarių skaičių")]
        public string RoomNr { get; set; }

        [Display(Name = "Sklypo plotas(kv/m)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite sklypo plotą")]
        public string PlotArea { get; set; }

        [Display(Name = "Aukštų skaičius")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite aukštų skaičių")]
        public string FloorNr { get; set; }

        [Display(Name = "Metai")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite pastato metus")]
        public string YearBuilt { get; set; }

        [Display(Name = "Tipas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite pastato tipą")]
        public string BuildingType { get; set; }

        [Display(Name = "Įrengimas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Aprašykite namo įrengimą")]
        public string HouseEquipment { get; set; }

        [Display(Name = "Šildymo tipas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite šildymo tipą")]
        public string HeatingType { get; set; }

        [Display(Name = "Papildoma informacija")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite papildoma informaciją")]
        public string AdditonalInfo { get; set; }

    }
}