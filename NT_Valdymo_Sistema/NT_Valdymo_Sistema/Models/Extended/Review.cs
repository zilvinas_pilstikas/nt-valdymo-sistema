﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NT_Valdymo_Sistema.Models
{
    [MetadataType(typeof(ReviewMetadata))]
    public partial class Review
    {
    }
    public class ReviewMetadata
    {
        [Display(Name = "Prašymo data")]
        public Nullable<System.DateTime> RequestDate { get; set; }

        [Display(Name = "Įveskite sumą (€)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite sumą!")]
        [Range(0, float.MaxValue, ErrorMessage = "Įveskite teigiamą skaičių!")]
        public Nullable<double> Sum { get; set; }

        [Display(Name = "Pateikite komentarus")]
        public string ReviewText { get; set; }

        [Display(Name = "Užklauos ID")]
        public int ReviewID { get; set; }

        [Display(Name = "Būsena")]
        public string Status { get; set; }
    }
}