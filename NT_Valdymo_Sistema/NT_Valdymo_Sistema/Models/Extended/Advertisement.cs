﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace NT_Valdymo_Sistema.Models
{
    [MetadataType(typeof(AdvertisementMetadata))]
    public partial class Advertisement
    {

    }

    public class AdvertisementMetadata
    {
        [Display(Name = "Kaina(€)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite kainą")]
        public double Price { get; set; }
        [Display(Name = "Telefono numeris")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite telefono numerį")]
        public string PhoneNr { get; set; }
        [Display(Name = "Nuoma?")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pasirinkite variantą")]
        public Nullable<int> Rent { get; set; }
        [Display(Name ="Įkelti nuotrauką")]
        public string ImagePath { get; set; }
    }
}